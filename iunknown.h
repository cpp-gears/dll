#pragma once
#include <memory>

struct iunknown {
	template <typename I>
	inline bool QueryInterface(std::shared_ptr<I>& pI) {
		if (std::is_base_of<iunknown, I>::value) {
			auto so{ shared() };
			pI = std::shared_ptr<I>(so, dynamic_cast<I*>(so.get()));
			return true;
		}
		return false;
	}
private:
	virtual std::shared_ptr<iunknown> shared() = 0;
};