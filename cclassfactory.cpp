#include "cclassfactory.h"
#include <dlfcn.h>

ssize_t cclassfactory::Add(const std::string& module_class, const std::string& module_pathname) {
	return dllModules.emplace(module_class, module_t{ module_pathname }).second ? 0 : -EEXIST;
}

ssize_t cclassfactory::CreateObject(const std::string& module_class, std::shared_ptr<iunknown>& pI, const std::unordered_map<std::string, std::string>& ctorargs) {
	if (auto&& so{ dllModules.find(module_class) }; so != dllModules.end()) {
		return so->second.create(module_class, pI, ctorargs);
	}
	return -ENOENT;
}


cclassfactory::module_t::~module_t() {
	if (soHandle) {
		dlclose(soHandle);
	}
}

ssize_t cclassfactory::module_t::create(const std::string& module_class, std::shared_ptr<iunknown>& pI, const std::unordered_map<std::string, std::string>& ctorargs) {
	if (soHandle == nullptr && !soName.empty()) {
		soHandle = dlopen(soName.c_str(), RTLD_NOW);
		if (soHandle == nullptr) {
			printf("[ ERROR ] module %s, class %s. %s\n", soName.c_str(), module_class.c_str(), dlerror());
			return -ENOEXEC;
		}
		if (soFactory = (__DllClassFactory)dlsym(soHandle, "DllClassFactory"); soFactory == nullptr) {
			printf("[ ERROR ] module %s, class %s. %s\n", soName.c_str(), module_class.c_str(), dlerror());
			dlclose(soHandle);
			soHandle = nullptr;
			return -ENOSYS;
		}
	}
	return (*soFactory)(pI, ctorargs, module_class) ? 0 : -ENOMEDIUM;
}

cclassfactory::cclassfactory() {

}

cclassfactory::~cclassfactory() {

}