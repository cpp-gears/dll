#pragma once
#include <string>
#include <unordered_map>
#include "iunknown.h"

extern "C"
{
	typedef bool (*__DllClassFactory)(std::shared_ptr<iunknown>& pI, const std::unordered_map<std::string, std::string>& ctorargs, const std::string& class_name);
}

class cclassfactory {
public:
	cclassfactory();
	~cclassfactory();
	ssize_t Add(const std::string& module_class, const std::string& module_pathname);
	ssize_t CreateObject(const std::string& module_class, std::shared_ptr<iunknown>& pI, const std::unordered_map<std::string, std::string>& ctorargs);

	template<typename CLS, typename ... CTOR_ARGS >
	static inline std::shared_ptr<iunknown> CreateObject(CTOR_ARGS&& ... ctor_args) {
		std::shared_ptr<CLS> object(new CLS(ctor_args...));
		std::shared_ptr<iunknown> pUnknow;
		object->QueryInterface(pUnknow);
		return pUnknow;
	}

	template<typename IFACE,typename BASE = iunknown>
	static inline std::shared_ptr<IFACE> InvokeInterface(const std::shared_ptr<BASE>& pBase) {
		if (std::shared_ptr<IFACE> pIface; pBase && dynamic_cast<iunknown*>(pBase.get())->QueryInterface<IFACE>(pIface)) {
			return pIface;
		}
		return {};
	}

private:
	struct module_t {
		std::string soName;
		void* soHandle{ nullptr };
		__DllClassFactory		soFactory{ nullptr };
		~module_t();
		ssize_t create(const std::string& module_class, std::shared_ptr<iunknown>& pI, const std::unordered_map<std::string, std::string>& ctorargs);
	};
	std::unordered_map<std::string, module_t>	dllModules;
};