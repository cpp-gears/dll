# Dll class factory
---

## Interface declaration example
---
```cpp
struct isapi : virtual iunknown {
	virtual ssize_t foo() = 0;
};
```

## Class export example
---
```cpp
#include "interfaces/ifoo.h"

class cfoo : virtual public iunknown, virtual public ifoo, public std::enable_shared_from_this<cfoo> {
public:
	cfoo(const std::initializer_list<std::string>& args){}
	virtual ~cfoo(){}
	virtual ssize_t foo() override{return 1;}
private:
	virtual std::shared_ptr<iunknown> shared() override {
		return std::shared_ptr<iunknown>(shared_from_this(), (iunknown*)this);
	}
};


extern "C" {
    dllexport bool DllClassFactory(std::shared_ptr<iunknown>& pI, const std::initializer_list<std::string>& ctorargs, const std::string& class_name) {
        std::shared_ptr<cfoo> object(new cfoo(ctorargs));
        return object->QueryInterface(pI);
    }
}
```

## Using example
---

```cpp

cclassfactory dllFactory;
dllFactory.Add("cfoo","libfoo.so");
dllFactory.Add(... other class dll);
...

std::shared_ptr<ifoo> foo;
if(std::shared_ptr<iunknown> obj; dllFactory.CreateObject(obj,{"arg 1"})) {
	obj->QueryInterface(foo);
}

```